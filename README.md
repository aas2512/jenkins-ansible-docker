# PIPELINE USANDO JENKINS, DOCKER, ANSIBLE

## Na caminhada de aprendizado as ferramentas que englobam a cultura devops resolvi aprofundar e montar um pipeline no Jenkins

1. Criação de duas EC2 na AWS;
2. Instalação e configuração do Jenkins/Maven/Ansible e dependencias;
3. Criação do Dockerfile;
O Passo a passo do pipeline:
3. Pega repositorio no gitlab;
4. Compila com o Maven;
5. Builda a imagem docker com o package;
6. Faz docker push;
7. roda o playbook no ansible para criar a estrutura na Instancia;
 - Instala python, docker-py, docker, e faz o deploy da imagem no docker;
8. Manda noticações do pipeline diretamente para o Slack com o status do pipeline;
9. Configurado para quando for feito push, automaticamente rodar o pipeline;

Links Uteis: 
Como instalar o Docker: https://docs.docker.com/engine/install/ 

Como instalar o Jenkins: https://www.jenkins.io/doc/book/installing/ 

GitLab Plugin para Jenkins: https://plugins.jenkins.io/gitlab-plugin/ 

GitLab Webhook: https://docs.gitlab.com/ee/integration/jenkins.html

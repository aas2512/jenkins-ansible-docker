#!/bin/bash

MESSAGE="DevOps Learn"

if [ -z "$1"  ]; then
	echo "Mensagem padrão: DevOps Learn"
	echo "Para alterar mensagem passe como argumento"
#echo $MESSAGE
else
	MESSAGE=$1

fi
echo ""
echo ""
echo "Mensagem no commit:" ${MESSAGE}
git add *
git commit -m "${MESSAGE}"
git push
